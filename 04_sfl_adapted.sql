/************************************************************************************
 QFF varaibles.
 ************************************************************************************/
create or replace table `gcp-wow-rwds-ai-beh-seg-dev.engagement_weights.redemption_qff` as with groupselect as(
        select crn,
            max(redemption_date) as recent_date,
            sum(redeem_value) as total_redeem_value
        from `gcp-wow-rwds-ai-beh-seg-dev.member_engagement_score.redemption_history_setting`
        where pref = "QFF"
            and redemption_date between '2021-04-04' and '2022-04-03'
        group by 1
    )
select crn,
    total_redeem_value,
    DATE_DIFF('2022-04-03', recent_date, DAY) AS recency
from groupselect
order by recency;
/************************************************************************************
 BFC varaibles.
 ************************************************************************************/
create or replace table `gcp-wow-rwds-ai-beh-seg-dev.engagement_weights.redemption_bfc` as
select crn,
    sum(redeem_value) as total_redeem_value,
    avg(redeem_value) as average_redeem_value
from `gcp-wow-rwds-ai-beh-seg-dev.member_engagement_score.redemption_history_setting`
where pref = "BFC"
    and redemption_date between '2021-11-01' and '2021-12-31'
group by 1
order by 2;
/************************************************************************************
 VOU varaibles.
 ************************************************************************************/
create or replace table `gcp-wow-rwds-ai-beh-seg-dev.engagement_weights.redemption_vou` as with groupselect as(
        select crn,
            max(redemption_date) as recent_date,
            sum(redeem_value) as total_redeem_value
        from `gcp-wow-rwds-ai-beh-seg-dev.member_engagement_score.redemption_history_setting`
        where pref = "VOU"
            and redemption_date between '2021-04-04' and '2022-04-03'
        group by 1
    )
select crn,
    total_redeem_value,
    DATE_DIFF('2022-04-03', recent_date, DAY) AS recency
from groupselect
order by recency;
/**************************************************************************************
 Set binary flags for BFC and QFF.
 **************************************************************************************/
update `gcp-wow-rwds-ai-beh-seg-dev.engagement_weights.redemption_qff`
set qff_redeemer_flag = 0
where true;
update `gcp-wow-rwds-ai-beh-seg-dev.engagement_weights.redemption_qff`
set qff_redeemer_flag = 1
where recency <= 124
    and total_redeem_value > 10;
update `gcp-wow-rwds-ai-beh-seg-dev.engagement_weights.redemption_bfc`
set bfc_redeemer_flag = 0
where true;
update `gcp-wow-rwds-ai-beh-seg-dev.engagement_weights.redemption_bfc`
set bfc_redeemer_flag = 1
where average_redeem_value >= 20
    and total_redeem_value >= 30;
update `gcp-wow-rwds-ai-beh-seg-dev.engagement_weights.redemption_vou`
set vou_redeemer_flag = 0
where true;
update `gcp-wow-rwds-ai-beh-seg-dev.engagement_weights.redemption_vou`
set vou_redeemer_flag = 1
where recency <= 141
    and total_redeem_value > 10;
/**************************************************************************************
 Create base table for weights caculation.
 **************************************************************************************/
create or replace table `gcp-wow-rwds-ai-beh-seg-dev.engagement_weights.redemption_sfl` as with base as (
        select crn,
            has_rwdapp_flag,
            edm_optin_flag,
            emails_open_l8w_flag,
            emails_click_l8w_flag,
            emails_activated_l8w_flag,
            scan_supers_l12w_flag,
            scan_bigw_l26w_flag,
            scan_bws_l26w_flag,
            scan_caltex_l12w_flag,
            scan_eg_l12w_flag,
            case
                when boosted_supers_l8w_flag > 0 then 1
                else 0
            end as boosted_supers_l8w_flag,
            case
                when boosted_bigw_l8w_flag > 0 then 1
                else 0
            end as boosted_bigw_l8w_flag,
            case
                when boosted_bws_l8w_flag > 0 then 1
                else 0
            end as boosted_bws_l8w_flag,
            case
                when boosted_caltex_l8w_flag > 0 then 1
                else 0
            end as boosted_caltex_l8w_flag,
            case
                when boosted_fuelco_l8w_flag > 0 then 1
                else 0
            end as boosted_fuelco_l8w_flag,
            case
                when boosted_other_l8w_flag > 0 then 1
                else 0
            end as boosted_other_l8w_flag,
            linked_wowonline_flag,
            linked_bigwonline_flag,
            linked_wowapp_flag,
            linked_bws_onlineapp_flag,
            linked_origin_flag,
            linked_bupa_flag,
            linked_caltex_flag,
            linked_telco_onlineapp_flag,
            linked_eg_flag,
            linked_money_flag,
            linked_superpharm_flag,
            linked_petculture_flag,
            linked_giftcard_flag,
            linked_healthylife_flag,
            insurance_flag,
            rapp_browse_l8w_flag,
            rapp_activated_l8w_flag
        from `gcp-wow-rwds-ai-beh-seg-dev.member_engagement_score.member_scores_hist` eng
        where eng.fw_end_date = '2022-04-03'
        group by 1,
            2,
            3,
            4,
            5,
            6,
            7,
            8,
            9,
            10,
            11,
            12,
            13,
            14,
            15,
            16,
            17,
            18,
            19,
            20,
            21,
            22,
            23,
            24,
            25,
            26,
            27,
            28,
            29,
            30,
            31,
            32,
            33,
            34
    )
select distinct cast('2022-04-03' as date) as fw_end_date,
    base.*,
    bfc_redeemer_flag,
    qff_redeemer_flag,
    vou_redeemer_flag
from base
    left join `gcp-wow-rwds-ai-beh-seg-dev.engagement_weights.redemption_qff` qff on base.crn = qff.crn
    left join `gcp-wow-rwds-ai-beh-seg-dev.engagement_weights.redemption_bfc` bfc on base.crn = bfc.crn
    left join `gcp-wow-rwds-ai-beh-seg-dev.engagement_weights.redemption_vou` vou on base.crn = vou.crn;
update `gcp-wow-rwds-ai-beh-seg-dev.engagement_weights.redemption_sfl`
set qff_redeemer_flag = 0
where qff_redeemer_flag is null;
update `gcp-wow-rwds-ai-beh-seg-dev.engagement_weights.redemption_sfl`
set bfc_redeemer_flag = 0
where bfc_redeemer_flag is null;
update `gcp-wow-rwds-ai-beh-seg-dev.engagement_weights.redemption_sfl`
set vou_redeemer_flag = 0
where vou_redeemer_flag is null;
/**************************************************************************************
 Transpose current pipeline weights and caculate current pipeline engagement score.
 **************************************************************************************/
create or replace table `gcp-wow-rwds-ai-beh-seg-dev.engagement_weights.current_weights_t` as
select cast("2022-04-03" as date) as fw_end_date,
    sum(
        case
            when variables = "emails_activated_l8w_flag" then adjusted_weighted_score
            else null
        end
    ) as weights_emails_activated_l8w_flag,
    sum(
        case
            when variables = "emails_open_l8w_flag" then adjusted_weighted_score
            else null
        end
    ) as weights_emails_open_l8w_flag,
    sum(
        case
            when variables = "emails_click_l8w_flag" then adjusted_weighted_score
            else null
        end
    ) as weights_emails_click_l8w_flag,
    sum(
        case
            when variables = "rapp_browse_l8w_flag" then adjusted_weighted_score
            else null
        end
    ) as weights_rapp_browse_l8w_flag,
    sum(
        case
            when variables = "rapp_activated_l8w_flag" then adjusted_weighted_score
            else null
        end
    ) as weights_rapp_activated_l8w_flag,
    sum(
        case
            when variables = "edm_optin_flag" then adjusted_weighted_score
            else null
        end
    ) as weights_edm_optin_flag,
    sum(
        case
            when variables = "has_rwdapp_flag" then adjusted_weighted_score
            else null
        end
    ) as weights_has_rwdapp_flag,
    sum(
        case
            when variables = "linked_wowonline_flag" then adjusted_weighted_score
            else null
        end
    ) as weights_linked_wowonline_flag,
    sum(
        case
            when variables = "linked_bigwonline_flag" then adjusted_weighted_score
            else null
        end
    ) as weights_linked_bigwonline_flag,
    sum(
        case
            when variables = "linked_wowapp_flag" then adjusted_weighted_score
            else null
        end
    ) as weights_linked_wowapp_flag,
    sum(
        case
            when variables = "linked_bws_onlineapp_flag" then adjusted_weighted_score
            else null
        end
    ) as weights_linked_bws_onlineapp_flag,
    sum(
        case
            when variables = "boosted_supers_l8w_flag" then adjusted_weighted_score
            else null
        end
    ) as weights_boosted_supers_l8w_flag,
    sum(
        case
            when variables = "boosted_bigw_l8w_flag" then adjusted_weighted_score
            else null
        end
    ) as weights_boosted_bigw_l8w_flag,
    sum(
        case
            when variables = "boosted_bws_l8w_flag" then adjusted_weighted_score
            else null
        end
    ) as weights_boosted_bws_l8w_flag,
    sum(
        case
            when variables = "boosted_caltex_l8w_flag" then adjusted_weighted_score
            else null
        end
    ) as weights_boosted_caltex_l8w_flag,
    sum(
        case
            when variables = "boosted_fuelco_l8w_flag" then adjusted_weighted_score
            else null
        end
    ) as weights_boosted_fuelco_l8w_flag,
    sum(
        case
            when variables = "boosted_other_l8w_flag" then adjusted_weighted_score
            else null
        end
    ) as weights_boosted_other_l8w_flag,
    sum(
        case
            when variables = "scan_supers_l12w_flag" then adjusted_weighted_score
            else null
        end
    ) as weights_scan_supers_l12w_flag,
    sum(
        case
            when variables = "scan_bigw_l26w_flag" then adjusted_weighted_score
            else null
        end
    ) as weights_scan_bigw_l26w_flag,
    sum(
        case
            when variables = "scan_bws_l26w_flag" then adjusted_weighted_score
            else null
        end
    ) as weights_scan_bws_l26w_flag,
    sum(
        case
            when variables = "scan_caltex_l12w_flag" then adjusted_weighted_score
            else null
        end
    ) as weights_scan_caltex_l12w_flag,
    sum(
        case
            when variables = "scan_eg_l12w_flag" then adjusted_weighted_score
            else null
        end
    ) as weights_scan_eg_l12w_flag,
    sum(
        case
            when variables = "linked_origin_flag" then adjusted_weighted_score
            else null
        end
    ) as weights_linked_origin_flag,
    sum(
        case
            when variables = "linked_bupa_flag" then adjusted_weighted_score
            else null
        end
    ) as weights_linked_bupa_flag,
    sum(
        case
            when variables = "linked_caltex_flag" then adjusted_weighted_score
            else null
        end
    ) as weights_linked_caltex_flag,
    sum(
        case
            when variables = "linked_telco_onlineapp_flag" then adjusted_weighted_score
            else null
        end
    ) as weights_linked_telco_onlineapp_flag,
    sum(
        case
            when variables = "linked_eg_flag" then adjusted_weighted_score
            else null
        end
    ) as weights_linked_eg_flag,
    sum(
        case
            when variables = "linked_money_flag" then adjusted_weighted_score
            else null
        end
    ) as weights_linked_money_flag,
    sum(
        case
            when variables = "linked_superpharm_flag" then adjusted_weighted_score
            else null
        end
    ) as weights_linked_superpharm_flag,
    sum(
        case
            when variables = "linked_petculture_flag" then adjusted_weighted_score
            else null
        end
    ) as weights_linked_petculture_flag,
    sum(
        case
            when variables = "linked_giftcard_flag" then adjusted_weighted_score
            else null
        end
    ) as weights_linked_giftcard_flag,
    sum(
        case
            when variables = "linked_healthylife_flag" then adjusted_weighted_score
            else null
        end
    ) as weights_linked_healthylife_flag,
    sum(
        case
            when variables = "insurance_flag" then adjusted_weighted_score
            else null
        end
    ) as weights_insurance_flag
from `gcp-wow-rwds-ai-beh-seg-dev.engagement_weights.current_weights`;
create or replace table `gcp-wow-rwds-ai-beh-seg-dev.engagement_weights.current_scores` as WITH TABLE_1 AS (
        SELECT eng.fw_end_date,
            crn,
            emails_activated_l8w_flag * weights_emails_activated_l8w_flag as emails_activated_l8w_flag,
            emails_open_l8w_flag * weights_emails_open_l8w_flag as emails_open_l8w_flag,
            emails_click_l8w_flag * weights_emails_click_l8w_flag as emails_click_l8w_flag,
            rapp_browse_l8w_flag * weights_rapp_browse_l8w_flag as rapp_browse_l8w_flag,
            rapp_activated_l8w_flag * weights_rapp_activated_l8w_flag as rapp_activated_l8w_flag,
            edm_optin_flag * weights_edm_optin_flag as edm_optin_flag,
            has_rwdapp_flag * weights_has_rwdapp_flag as has_rwdapp_flag,
            linked_wowonline_flag * weights_linked_wowonline_flag as linked_wowonline_flag,
            linked_bigwonline_flag * weights_linked_bigwonline_flag as linked_bigwonline_flag,
            linked_wowapp_flag * weights_linked_wowapp_flag as linked_wowapp_flag,
            linked_bws_onlineapp_flag * weights_linked_bws_onlineapp_flag as linked_bws_onlineapp_flag,
            case
                when boosted_supers_l8w_flag > 0 then 1
                else 0
            end * weights_boosted_supers_l8w_flag as boosted_supers_l8w_flag,
            case
                when boosted_bigw_l8w_flag > 0 then 1
                else 0
            end * weights_boosted_bigw_l8w_flag as boosted_bigw_l8w_flag,
            case
                when boosted_bws_l8w_flag > 0 then 1
                else 0
            end * weights_boosted_bws_l8w_flag as boosted_bws_l8w_flag,
            case
                when boosted_caltex_l8w_flag > 0 then 1
                else 0
            end * weights_boosted_caltex_l8w_flag as boosted_caltex_l8w_flag,
            case
                when boosted_fuelco_l8w_flag > 0 then 1
                else 0
            end * weights_boosted_fuelco_l8w_flag as boosted_fuelco_l8w_flag,
            case
                when boosted_other_l8w_flag > 0 then 1
                else 0
            end * weights_boosted_other_l8w_flag as boosted_other_l8w_flag,
            scan_supers_l12w_flag * weights_scan_supers_l12w_flag as scan_supers_l12w_flag,
            scan_bigw_l26w_flag * weights_scan_bigw_l26w_flag as scan_bigw_l26w_flag,
            scan_bws_l26w_flag * weights_scan_bws_l26w_flag as scan_bws_l26w_flag,
            scan_caltex_l12w_flag * weights_scan_caltex_l12w_flag as scan_caltex_l12w_flag,
            scan_eg_l12w_flag * weights_scan_eg_l12w_flag as scan_eg_l12w_flag,
            linked_origin_flag * weights_linked_origin_flag as linked_origin_flag,
            linked_bupa_flag * weights_linked_bupa_flag as linked_bupa_flag,
            linked_caltex_flag * weights_linked_caltex_flag as linked_caltex_flag,
            linked_telco_onlineapp_flag * weights_linked_telco_onlineapp_flag as linked_telco_onlineapp_flag,
            linked_eg_flag * weights_linked_eg_flag as linked_eg_flag,
            linked_money_flag * weights_linked_money_flag as linked_money_flag,
            linked_superpharm_flag * weights_linked_superpharm_flag as linked_superpharm_flag,
            linked_petculture_flag * weights_linked_petculture_flag as linked_petculture_flag,
            linked_giftcard_flag * weights_linked_giftcard_flag as linked_giftcard_flag,
            linked_healthylife_flag * weights_linked_healthylife_flag as linked_healthylife_flag,
            insurance_flag * weights_insurance_flag as insurance_flag
        from `gcp-wow-rwds-ai-beh-seg-dev.member_engagement_score.member_scores_hist` eng
            left join `gcp-wow-rwds-ai-beh-seg-dev.engagement_weights.current_weights_t` on 1 = 1
        where eng.fw_end_date = "2022-04-03"
    )
SELECT *,
    emails_activated_l8w_flag + emails_open_l8w_flag + emails_click_l8w_flag + rapp_browse_l8w_flag + rapp_activated_l8w_flag + edm_optin_flag + has_rwdapp_flag + linked_wowonline_flag + linked_bigwonline_flag + linked_wowapp_flag + linked_bws_onlineapp_flag + boosted_supers_l8w_flag + boosted_bigw_l8w_flag + boosted_bws_l8w_flag + boosted_caltex_l8w_flag + boosted_fuelco_l8w_flag + boosted_other_l8w_flag + scan_supers_l12w_flag + scan_bigw_l26w_flag + scan_bws_l26w_flag + scan_caltex_l12w_flag + scan_eg_l12w_flag + linked_origin_flag + linked_bupa_flag + linked_caltex_flag + linked_telco_onlineapp_flag + linked_eg_flag + linked_money_flag + linked_superpharm_flag + linked_petculture_flag + linked_giftcard_flag + linked_healthylife_flag + insurance_flag as adjusted_total_engagement_score
from TABLE_1;
/**************************************************************************************
 Redemption features belong to EI group.
 After weights caculation, get testing weights.
 **************************************************************************************/
create or replace table `gcp-wow-rwds-ai-beh-seg-dev.engagement_weights.sfl_weights_t` as
select cast("2022-04-03" as date) as fw_end_date,
    sum(
        case
            when variables = "emails_activated_l8w_flag" then adjusted_weighted_score
            else null
        end
    ) as weights_emails_activated_l8w_flag,
    sum(
        case
            when variables = "emails_open_l8w_flag" then adjusted_weighted_score
            else null
        end
    ) as weights_emails_open_l8w_flag,
    sum(
        case
            when variables = "emails_click_l8w_flag" then adjusted_weighted_score
            else null
        end
    ) as weights_emails_click_l8w_flag,
    sum(
        case
            when variables = "rapp_browse_l8w_flag" then adjusted_weighted_score
            else null
        end
    ) as weights_rapp_browse_l8w_flag,
    sum(
        case
            when variables = "rapp_activated_l8w_flag" then adjusted_weighted_score
            else null
        end
    ) as weights_rapp_activated_l8w_flag,
    sum(
        case
            when variables = "edm_optin_flag" then adjusted_weighted_score
            else null
        end
    ) as weights_edm_optin_flag,
    sum(
        case
            when variables = "has_rwdapp_flag" then adjusted_weighted_score
            else null
        end
    ) as weights_has_rwdapp_flag,
    sum(
        case
            when variables = "linked_wowonline_flag" then adjusted_weighted_score
            else null
        end
    ) as weights_linked_wowonline_flag,
    sum(
        case
            when variables = "linked_bigwonline_flag" then adjusted_weighted_score
            else null
        end
    ) as weights_linked_bigwonline_flag,
    sum(
        case
            when variables = "linked_wowapp_flag" then adjusted_weighted_score
            else null
        end
    ) as weights_linked_wowapp_flag,
    sum(
        case
            when variables = "linked_bws_onlineapp_flag" then adjusted_weighted_score
            else null
        end
    ) as weights_linked_bws_onlineapp_flag,
    sum(
        case
            when variables = "boosted_supers_l8w_flag" then adjusted_weighted_score
            else null
        end
    ) as weights_boosted_supers_l8w_flag,
    sum(
        case
            when variables = "boosted_bigw_l8w_flag" then adjusted_weighted_score
            else null
        end
    ) as weights_boosted_bigw_l8w_flag,
    sum(
        case
            when variables = "boosted_bws_l8w_flag" then adjusted_weighted_score
            else null
        end
    ) as weights_boosted_bws_l8w_flag,
    sum(
        case
            when variables = "boosted_caltex_l8w_flag" then adjusted_weighted_score
            else null
        end
    ) as weights_boosted_caltex_l8w_flag,
    sum(
        case
            when variables = "boosted_fuelco_l8w_flag" then adjusted_weighted_score
            else null
        end
    ) as weights_boosted_fuelco_l8w_flag,
    sum(
        case
            when variables = "boosted_other_l8w_flag" then adjusted_weighted_score
            else null
        end
    ) as weights_boosted_other_l8w_flag,
    sum(
        case
            when variables = "scan_supers_l12w_flag" then adjusted_weighted_score
            else null
        end
    ) as weights_scan_supers_l12w_flag,
    sum(
        case
            when variables = "scan_bigw_l26w_flag" then adjusted_weighted_score
            else null
        end
    ) as weights_scan_bigw_l26w_flag,
    sum(
        case
            when variables = "scan_bws_l26w_flag" then adjusted_weighted_score
            else null
        end
    ) as weights_scan_bws_l26w_flag,
    sum(
        case
            when variables = "scan_caltex_l12w_flag" then adjusted_weighted_score
            else null
        end
    ) as weights_scan_caltex_l12w_flag,
    sum(
        case
            when variables = "scan_eg_l12w_flag" then adjusted_weighted_score
            else null
        end
    ) as weights_scan_eg_l12w_flag,
    sum(
        case
            when variables = "linked_origin_flag" then adjusted_weighted_score
            else null
        end
    ) as weights_linked_origin_flag,
    sum(
        case
            when variables = "linked_bupa_flag" then adjusted_weighted_score
            else null
        end
    ) as weights_linked_bupa_flag,
    sum(
        case
            when variables = "linked_caltex_flag" then adjusted_weighted_score
            else null
        end
    ) as weights_linked_caltex_flag,
    sum(
        case
            when variables = "linked_telco_onlineapp_flag" then adjusted_weighted_score
            else null
        end
    ) as weights_linked_telco_onlineapp_flag,
    sum(
        case
            when variables = "linked_eg_flag" then adjusted_weighted_score
            else null
        end
    ) as weights_linked_eg_flag,
    sum(
        case
            when variables = "linked_money_flag" then adjusted_weighted_score
            else null
        end
    ) as weights_linked_money_flag,
    sum(
        case
            when variables = "linked_superpharm_flag" then adjusted_weighted_score
            else null
        end
    ) as weights_linked_superpharm_flag,
    sum(
        case
            when variables = "linked_petculture_flag" then adjusted_weighted_score
            else null
        end
    ) as weights_linked_petculture_flag,
    sum(
        case
            when variables = "linked_giftcard_flag" then adjusted_weighted_score
            else null
        end
    ) as weights_linked_giftcard_flag,
    sum(
        case
            when variables = "linked_healthylife_flag" then adjusted_weighted_score
            else null
        end
    ) as weights_linked_healthylife_flag,
    sum(
        case
            when variables = "insurance_flag" then adjusted_weighted_score
            else null
        end
    ) as weights_insurance_flag,
    sum(
        case
            when variables = "bfc_redeemer_flag" then adjusted_weighted_score
            else null
        end
    ) as weights_bfc_redeemer_flag,
    sum(
        case
            when variables = "qff_redeemer_flag" then adjusted_weighted_score
            else null
        end
    ) as weights_qff_redeemer_flag,
    sum(
        case
            when variables = "vou_redeemer_flag" then adjusted_weighted_score
            else null
        end
    ) as weights_vou_redeemer_flag
from `gcp-wow-rwds-ai-beh-seg-dev.engagement_weights.sfl_weights`;
/**************************************************************************************
 Get testing scores.
 **************************************************************************************/
create or replace table `gcp-wow-rwds-ai-beh-seg-dev.engagement_weights.sfl_scores` as WITH TABLE_1 AS (
        SELECT eng.fw_end_date,
            crn,
            emails_activated_l8w_flag * weights_emails_activated_l8w_flag as emails_activated_l8w_flag,
            emails_open_l8w_flag * weights_emails_open_l8w_flag as emails_open_l8w_flag,
            emails_click_l8w_flag * weights_emails_click_l8w_flag as emails_click_l8w_flag,
            rapp_browse_l8w_flag * weights_rapp_browse_l8w_flag as rapp_browse_l8w_flag,
            rapp_activated_l8w_flag * weights_rapp_activated_l8w_flag as rapp_activated_l8w_flag,
            edm_optin_flag * weights_edm_optin_flag as edm_optin_flag,
            has_rwdapp_flag * weights_has_rwdapp_flag as has_rwdapp_flag,
            linked_wowonline_flag * weights_linked_wowonline_flag as linked_wowonline_flag,
            linked_bigwonline_flag * weights_linked_bigwonline_flag as linked_bigwonline_flag,
            linked_wowapp_flag * weights_linked_wowapp_flag as linked_wowapp_flag,
            linked_bws_onlineapp_flag * weights_linked_bws_onlineapp_flag as linked_bws_onlineapp_flag,
            case
                when boosted_supers_l8w_flag > 0 then 1
                else 0
            end * weights_boosted_supers_l8w_flag as boosted_supers_l8w_flag,
            case
                when boosted_bigw_l8w_flag > 0 then 1
                else 0
            end * weights_boosted_bigw_l8w_flag as boosted_bigw_l8w_flag,
            case
                when boosted_bws_l8w_flag > 0 then 1
                else 0
            end * weights_boosted_bws_l8w_flag as boosted_bws_l8w_flag,
            case
                when boosted_caltex_l8w_flag > 0 then 1
                else 0
            end * weights_boosted_caltex_l8w_flag as boosted_caltex_l8w_flag,
            case
                when boosted_fuelco_l8w_flag > 0 then 1
                else 0
            end * weights_boosted_fuelco_l8w_flag as boosted_fuelco_l8w_flag,
            case
                when boosted_other_l8w_flag > 0 then 1
                else 0
            end * weights_boosted_other_l8w_flag as boosted_other_l8w_flag,
            scan_supers_l12w_flag * weights_scan_supers_l12w_flag as scan_supers_l12w_flag,
            scan_bigw_l26w_flag * weights_scan_bigw_l26w_flag as scan_bigw_l26w_flag,
            scan_bws_l26w_flag * weights_scan_bws_l26w_flag as scan_bws_l26w_flag,
            scan_caltex_l12w_flag * weights_scan_caltex_l12w_flag as scan_caltex_l12w_flag,
            scan_eg_l12w_flag * weights_scan_eg_l12w_flag as scan_eg_l12w_flag,
            linked_origin_flag * weights_linked_origin_flag as linked_origin_flag,
            linked_bupa_flag * weights_linked_bupa_flag as linked_bupa_flag,
            linked_caltex_flag * weights_linked_caltex_flag as linked_caltex_flag,
            linked_telco_onlineapp_flag * weights_linked_telco_onlineapp_flag as linked_telco_onlineapp_flag,
            linked_eg_flag * weights_linked_eg_flag as linked_eg_flag,
            linked_money_flag * weights_linked_money_flag as linked_money_flag,
            linked_superpharm_flag * weights_linked_superpharm_flag as linked_superpharm_flag,
            linked_petculture_flag * weights_linked_petculture_flag as linked_petculture_flag,
            linked_giftcard_flag * weights_linked_giftcard_flag as linked_giftcard_flag,
            linked_healthylife_flag * weights_linked_healthylife_flag as linked_healthylife_flag,
            insurance_flag * weights_insurance_flag as insurance_flag,
            bfc_redeemer_flag * weights_bfc_redeemer_flag as bfc_redeemer_flag,
            qff_redeemer_flag * weights_qff_redeemer_flag as qff_redeemer_flag,
            vou_redeemer_flag * weights_vou_redeemer_flag as vou_redeemer_flag
        from `gcp-wow-rwds-ai-beh-seg-dev.engagement_weights.redemption_sfl` eng
            left join `gcp-wow-rwds-ai-beh-seg-dev.engagement_weights.sfl_weights_t` on 1 = 1
        where eng.fw_end_date = '2022-04-03'
    )
SELECT *,
    emails_activated_l8w_flag + emails_open_l8w_flag + emails_click_l8w_flag + rapp_browse_l8w_flag + rapp_activated_l8w_flag + edm_optin_flag + has_rwdapp_flag + linked_wowonline_flag + linked_bigwonline_flag + linked_wowapp_flag + linked_bws_onlineapp_flag + boosted_supers_l8w_flag + boosted_bigw_l8w_flag + boosted_bws_l8w_flag + boosted_caltex_l8w_flag + boosted_fuelco_l8w_flag + boosted_other_l8w_flag + scan_supers_l12w_flag + scan_bigw_l26w_flag + scan_bws_l26w_flag + scan_caltex_l12w_flag + scan_eg_l12w_flag + linked_origin_flag + linked_bupa_flag + linked_caltex_flag + linked_telco_onlineapp_flag + linked_eg_flag + linked_money_flag + linked_superpharm_flag + linked_petculture_flag + linked_giftcard_flag + linked_healthylife_flag + insurance_flag + bfc_redeemer_flag + qff_redeemer_flag + vou_redeemer_flag as adjusted_total_engagement_score
from TABLE_1;
/**************************************************************************************
 Create compared table.
 **************************************************************************************/
create or replace table `gcp-wow-rwds-ai-beh-seg-dev.engagement_weights.compared_scores` as
select test.crn,
    qff_redeemer_flag,
    bfc_redeemer_flag,
    vou_redeemer_flag,
    test.adjusted_total_engagement_score as adapted_score,
    original.adjusted_total_engagement_score as current_score
from `gcp-wow-rwds-ai-beh-seg-dev.engagement_weights.sfl_scores` test
    left join `gcp-wow-rwds-ai-beh-seg-dev.engagement_weights.current_scores` original on test.crn = original.crn
group by 1,
    2,
    3,
    4,
    5,
    6;
delete from `gcp-wow-rwds-ai-beh-seg-dev.engagement_weights.compared_scores`
where current_score is null;
/**************************************************************************************
 Compared results.
 **************************************************************************************/
SELECT count(*)
FROM `gcp-wow-rwds-ai-beh-seg-dev.engagement_weights.compared_scores`
where (
        qff_redeemer_flag > 0
        and bfc_redeemer_flag > 0
        and vou_redeemer_flag > 0
    );
SELECT count(*)
FROM `gcp-wow-rwds-ai-beh-seg-dev.engagement_weights.compared_scores`
where adapted_score > current_score
    and (
        qff_redeemer_flag > 0
        and bfc_redeemer_flag > 0
        and vou_redeemer_flag > 0
    );