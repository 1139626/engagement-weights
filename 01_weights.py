# Import packages.
import gcsfs
import math
import numpy as np
import os
import pandas as pd
import pickle
import pyarrow
import time
from google.cloud import bigquery
from google.cloud import storage
from pyarrow import parquet as pq
from sklearn.linear_model import LinearRegression

# Define BigQuery function.


def sql2df(project, sql):
    bq = bigquery.Client(project=project)
    df = bq.query(sql).result().to_dataframe()
    return df


# Extract data.
dataframe = sql2df("gcp-wow-rwds-ai-beh-seg-dev",
                  '''
                  select has_rwdapp_flag, 
                   edm_optin_flag, 
                   emails_open_l8w_flag, 
                   emails_click_l8w_flag, 
                   emails_activated_l8w_flag, 
                   scan_supers_l12w_flag, 
                   scan_bigw_l26w_flag, 
                   scan_bws_l26w_flag, 
                   scan_caltex_l12w_flag, 
                   scan_eg_l12w_flag, 
                   case when boosted_supers_l8w_flag > 0 then 1 else 0 end as boosted_supers_l8w_flag, 
                   case when boosted_bigw_l8w_flag > 0 then 1 else 0 end as boosted_bigw_l8w_flag, 
                   case when boosted_bws_l8w_flag > 0 then 1 else 0 end as boosted_bws_l8w_flag, 
                   case when boosted_caltex_l8w_flag > 0 then 1 else 0 end as boosted_caltex_l8w_flag, 
                   case when boosted_fuelco_l8w_flag > 0 then 1 else 0 end as boosted_fuelco_l8w_flag, 
                   case when boosted_other_l8w_flag > 0 then 1 else 0 end as boosted_other_l8w_flag, 
                   linked_wowonline_flag, 
                   linked_bigwonline_flag, 
                   linked_wowapp_flag, 
                   linked_bws_onlineapp_flag, 
                   linked_origin_flag, 
                   linked_bupa_flag, 
                   linked_caltex_flag, 
                   linked_telco_onlineapp_flag, 
                   linked_eg_flag, 
                   linked_money_flag, 
                   linked_superpharm_flag, 
                   linked_petculture_flag, 
                   linked_giftcard_flag, 
                   linked_healthylife_flag, 
                   insurance_flag, 
                   rapp_browse_l8w_flag, 
                   rapp_activated_l8w_flag, 
                   count(distinct eng.crn) as count_crn, 
                   sum(cav.score_spend) as sum_cav 
                  from `gcp-wow-rwds-ai-beh-seg-dev.member_engagement_score.member_scores_hist` eng 
                   left join (
                   select crn, score_spend from `gcp-wow-rwds-ai-cfv-prod.vw_score_full.cfv_instore_segments` 
                   where ref_dt = '2021-07-04'
                   ) cav 
                   on cav.crn = eng.crn 
                   left join (
                   select crn, score_spend from `gcp-wow-rwds-ai-cfv-prod.vw_score_full.cfv_ecom_segments`
                   where ref_dt = '2021-07-04'
                   ) cave 
                   on cave.crn = eng.crn 
                   where eng.fw_end_date = '2021-06-27'
                   group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33
                   '''
                  )

# Drop useless variable combinations.
dataframe = dataframe[dataframe['sum_cav'].notna()].reset_index(drop=True)

# Filter useless variables.
checking_df = pd.DataFrame(dataframe.sum()).reset_index()
useful_variables = list(
    checking_df[checking_df[0] != 0].reset_index(drop=True)["index"])
group_lookup = pd.read_csv("grp_lkup.csv")
group_lookup = group_lookup[group_lookup['var'].isin(
    useful_variables)].reset_index(drop=True)

# CaLcuLate the positive flag number for each group of each variable combination.
groups = ["CI", "CP", "EI", "EP"]
for group in groups:
    variables = group_lookup.loc[group_lookup['code'] == group]["var"]
    variables_list = list(variables)
    df = dataframe[variables_list]
    dataframe[group] = df.sum(axis=1)

# CaLcuLate the total number of crns and the total cav of each group combination.
new_df = dataframe[["CI", "CP", "EI", "EP", "count_crn",
                    "sum_cav"]].groupby(["CI", "CP", "EI", "EP"]).sum()
new_df.rename(inplace=True, columns={
              "count_crn": "sum_count_crn", "sum_cav": "sum_sum_cav"})

# CaLcuLate the average value of cav for each group combination.
new_df["avg_sum_cav"] = new_df.sum_sum_cav/new_df.sum_count_crn
new_df.reset_index(inplace=True)

# Train a model matching each group combination to its average value of cav.
X = new_df[groups]
y = new_df[["avg_sum_cav"]]
group_reg = LinearRegression(fit_intercept=False).fit(X, y)

# Get 1st level coeffcients.
group_table = pd.concat(
    [pd.DataFrame(groups), pd.DataFrame(group_reg.coef_).T], axis=1)
group_table.columns = ["group", "coef"]
print(group_table)

# Save the 1st level model.
pkl_filename = "group_reg.pkl"
with open(pkl_filename, 'wb') as file:
    pickle.dump(group_reg, file)

results_list = []

for group in groups:

    # Get group variables as a list.
    variables = group_lookup.loc[group_lookup['code'] == group]["var"]
    variables_list = list(variables)

    # Take useful columns from the previous dataframe.
    pred_dataframe = dataframe.copy()
    pred_dataframe = pred_dataframe[variables_list +
                                    groups+["sum_cav", "count_crn"]]

    # Set the group values to 0 and use the 1st level model to predict results.
    pred_dataframe[group] = 0
    pred_dataframe["pred_results"] = pd.DataFrame(
        group_reg.predict(pred_dataframe[groups]))

    # Use average cav as a base to calculate the contribution from the group.
    pred_dataframe["avg_cav"] = pred_dataframe["sum_cav"] / \
        pred_dataframe["count_crn"]
    pred_dataframe['contributions'] = pred_dataframe["avg_cav"] - \
        pred_dataframe["pred_results"]
    pred_dataframe["contribution_cav"] = pred_dataframe['contributions'] * \
        pred_dataframe["count_crn"]

    # Calculate the contribution part of average cav value.
    gb_dataframe = pred_dataframe.groupby(variables_list).sum().reset_index()
    gb_dataframe['avg_cav'] = gb_dataframe['contribution_cav'] / \
        gb_dataframe['count_crn']

    # Train group level model.
    X = gb_dataframe[variables_list]
    y = gb_dataframe['avg_cav']
    nested_reg = LinearRegression(fit_intercept=False).fit(X, y)

    # Process group level coefficients.
    original_coef = list(nested_reg.coef_)
    negative_coef_nbr = sum(n < 0 for n in original_coef)
    if negative_coef_nbr > 0:
        min_original_coef = min(original_coef)
        distance = original_coef-min_original_coef
        min_new_coef = math.sqrt(abs(min_original_coef))
        adjusted_coef = distance+min_new_coef
        weights_within_group = adjusted_coef/sum(adjusted_coef)
    else:
        weights_within_group = original_coef/sum(original_coef)

    # Generate group result and append to result list.
    results = pd.concat([pd.DataFrame(variables_list),
                        pd.DataFrame(weights_within_group)], axis=1)
    results.columns = ["variables", "weights"]
    results["group"] = group
    results_list.append(results)

# Combine 4 groups together.
divided_results = pd.concat(results_list, ignore_index=True)
divided_results

# Calculate final results.
group_table["group_cap"] = group_table["coef"]/sum(group_table["coef"])
combined_results = divided_results.merge(group_table, on="group", how="left")
combined_results["weighted_score"] = combined_results["group_cap"] * \
    combined_results["weights"]*100
combined_results["adjusted_weighted_score"] = 100*pd.DataFrame(
    combined_results["weighted_score"]**(1/2))/sum(combined_results["weighted_score"]**(1/2))
combined_results["raw_score"] = 1

# Add ranks and ref_dt.
combined_results['rank'] = combined_results["adjusted_weighted_score"].rank(
    method='first', ascending=False)
combined_results['ref_dt'] = '2021-06-27'

# Generate output.
combined_results.to_csv("combined_results.csv", index=False)
# Export to `gcp-wow-rwds-ai-beh-seg-dev.member_engagement_score.weights_for_engagement_20220322`