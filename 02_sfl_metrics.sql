/*************************************************************************
 QFF
 *************************************************************************/
create or replace table `gcp-wow-rwds-ai-beh-seg-dev.engagement_weights.redemption_qff_redeem` as
select crn,
    sum(redeem_value) as total_redeem_value,
    avg(redeem_value) as average_redeem_value,
    max(redeem_value) as max_redeem_value,
    min(redeem_value) as min_redeem_value,
    count(*) as redeem_times
from `gcp-wow-rwds-ai-beh-seg-dev.member_engagement_score.redemption_history_setting`
where pref = "QFF"
    and redemption_date between '2021-04-04' and '2022-04-03'
group by 1
order by 2;
create or replace table `gcp-wow-rwds-ai-beh-seg-dev.engagement_weights.redemption_qff_recency` as with groupselect as(
        select crn,
            max(redemption_date) as recent_date
        from `gcp-wow-rwds-ai-beh-seg-dev.member_engagement_score.redemption_history_setting`
        where pref = "QFF"
            and redemption_date between "2021-04-04" and '2022-04-03'
        group by 1
    )
select crn,
    DATE_DIFF("2022-04-03", recent_date, DAY) AS recency
from groupselect
order by recency;
/*************************************************************************
 BFC
 *************************************************************************/
create or replace table `gcp-wow-rwds-ai-beh-seg-dev.engagement_weights.redemption_bfc_redeem` as
select crn,
    sum(redeem_value) as total_redeem_value,
    avg(redeem_value) as average_redeem_value,
    max(redeem_value) as max_redeem_value,
    min(redeem_value) as min_redeem_value,
    count(*) as redeem_times
from `gcp-wow-rwds-ai-beh-seg-dev.member_engagement_score.redemption_history_setting`
where pref = "BFC"
    and redemption_date between '2021-11-01' and '2021-12-31'
group by 1
order by 2;
/*************************************************************************
 VOU
 *************************************************************************/
create or replace table `gcp-wow-rwds-ai-beh-seg-dev.engagement_weights.redemption_vou_redeem` as
select crn,
    sum(redeem_value) as total_redeem_value,
    avg(redeem_value) as average_redeem_value,
    max(redeem_value) as max_redeem_value,
    min(redeem_value) as min_redeem_value,
    count(*) as redeem_times
from `gcp-wow-rwds-ai-beh-seg-dev.member_engagement_score.redemption_history_setting`
where pref = "VOU" 
    and redemption_date between "2021-04-04" and '2022-04-03'
group by 1
order by 2;
create or replace table `gcp-wow-rwds-ai-beh-seg-dev.engagement_weights.redemption_vou_recency` as with groupselect as(
        select crn,
            max(redemption_date) as recent_date
        from `gcp-wow-rwds-ai-beh-seg-dev.member_engagement_score.redemption_history_setting`
        where pref = "VOU"
            and redemption_date between "2021-04-04" and '2022-04-03'
        group by 1
    )
select crn,
    DATE_DIFF('2022-04-03', recent_date, DAY) AS recency
from groupselect
order by recency;